To install gem:    

Git clone bitbucket url    
`git clone git@bitbucket.org:taylorlab/taylorlib.git`

cd into taylorlib directory so you can see taylorlib.gemspec     

`gem build taylorlib.gemspec`    

`gem install taylorlib-0.1.gem`    

---------

To include in a Gemfile    
paste this line into Gemfile:     

`gem 'taylorlib', git:'git@bitbucket.org:taylorlab/taylorlib.git' `
